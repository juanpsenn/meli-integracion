# Create your views here.
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from api.models import MELIParams
from api.selectors import get_auth, get_token, get_orders, get_seller_data, get_billing_info
from api.serializers import MELIParamsSerializer
from api.services import update_code


class MELIParamsViewSet(ModelViewSet):
    queryset = MELIParams.objects.all()
    serializer_class = MELIParamsSerializer


class MELINotify(APIView):
    def get(self, request):
        code = request.query_params.get('code')
        update_code(code=code)
        get_token()
        return HttpResponseRedirect(redirect_to='/api/')


class MELIAuthApi(APIView):
    def get(self, request):
        return Response(get_auth())


class MELITokenApi(APIView):
    def get(self, request):
        return Response(get_token())


class MELIOrdersApi(APIView):
    def get(self, requests):
        orders = get_orders(**requests.query_params)
        if 'error' in orders and orders['errors'] == 'invalid_grant':
            get_token()
            orders = get_orders(**requests.query_params)
        return Response(orders)


class MELIWhoamIApi(APIView):
    def get(self, request):
        return Response(get_seller_data())


class MELIBillingInfoApi(APIView):
    def get(self, request, order_id):
        return Response(get_billing_info(order_id=order_id))

