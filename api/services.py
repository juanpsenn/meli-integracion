from api.models import MELIParams


def update_params(*, params: MELIParams, token, seller, refresh_token):
    params.token = token
    params.seller = seller
    params.refresh_token = refresh_token
    params.save()


def update_code(*, code):
    params = MELIParams.objects.all().last()
    params.code = code
    params.save()
