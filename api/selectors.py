import urllib.parse

import meli
import requests
from meli.rest import ApiException

from api.models import MELIParams
from api.services import update_params

TAXPAYER_TYPE = {'Monotributo': 'MO',
                 'IVA Responsable Inscripto': 'RI',
                 'IVA Exento': 'EX'}

DOC_TYPE = {'DNI': '',
            'CUIT': ''}


def get_auth():
    meliparams = get_params()
    params = urllib.parse.urlencode(
        {'response_type': 'code', 'client_id': meliparams.client,
         'redirect_uri': meliparams.redirect_uri})
    return 'https://auth.mercadolibre.com.ar/authorization?{}'.format(params)


def get_token():
    # Defining the host, defaults to https://api.mercadolibre.com
    # See configuration.py for a list of all supported configuration parameters.
    configuration = meli.Configuration(
        host="https://api.mercadolibre.com"
    )
    meliparams = get_params()
    # Enter a context with an instance of the API client
    with meli.ApiClient() as api_client:
        # Create an instance of the API class
        api_instance = meli.OAuth20Api(api_client)
        grant_type = 'authorization_code' if not meliparams.refresh_token else 'refresh_token'  # str
        client_id = meliparams.client  # Your client_id
        client_secret = meliparams.client_secret  # Your client_secret
        redirect_uri = meliparams.redirect_uri  # Your redirect_uri
        code = meliparams.code  # The parameter CODE
        refresh_token = meliparams.refresh_token  # Your refresh_token

    try:
        # Request Access Token with refresh token
        if refresh_token:
            api_response = api_instance.get_token(grant_type=grant_type, client_id=client_id,
                                                  client_secret=client_secret,
                                                  redirect_uri=redirect_uri, code=code, refresh_token=refresh_token)
        else:
            api_response = api_instance.get_token(grant_type=grant_type, client_id=client_id,
                                                  client_secret=client_secret,
                                                  redirect_uri=redirect_uri, code=code)
        update_params(params=meliparams,
                      token=api_response.get('access_token'),
                      refresh_token=api_response.get('refresh_token'),
                      seller=api_response.get('user_id'))
        return api_response
    except ApiException as e:
        return "Exception when calling OAuth20Api->get_token: %s\n" % e


def get_orders(**kwargs):
    meliparams = get_params()
    test = [4185481150, 2]
    params = {'access_token': meliparams.token,
              'seller': meliparams.seller,
              'offset': kwargs.get('offset') or 0,
              'limit': kwargs.get('limit') or 50}
    params.update(kwargs)
    r = requests.get('https://api.mercadolibre.com/orders/search', params=params)
    orders = r.json()
    for order in orders.get('results'):
        if order.get('id') in test:
            order['facturada'] = 'true'
    return orders


def get_billing_info(*, order_id):
    meliparams = get_params()
    params = {'access_token': meliparams.token}
    r = requests.get('https://api.mercadolibre.com/orders/{order}/billing_info'.format(order=order_id), params=params)
    data = r.json()
    info = {d['type']: d['value'] for d in r.json().get('additional_info')}
    doc_type = DOC_TYPE[data.get('doc_type')]
    doc_number = data.get('doc_type')
    return r.json() if r.status_code == 200 else "No orders found"


def get_params():
    return MELIParams.objects.all().last()
