from rest_framework import serializers

from api.models import MELIParams


class MELIParamsSerializer(serializers.ModelSerializer):
    class Meta:
        model = MELIParams
        fields = '__all__'
