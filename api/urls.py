from django.urls import path, include
from rest_framework import routers

from api.views import MELIAuthApi, MELITokenApi, MELIOrdersApi, MELIWhoamIApi, MELIBillingInfoApi, \
    MELIParamsViewSet, MELINotify

router = routers.DefaultRouter()

router.register('params', MELIParamsViewSet)

urlpatterns = [
    path('auth/', MELIAuthApi.as_view()),
    path('token/', MELITokenApi.as_view()),
    path('orders/', MELIOrdersApi.as_view()),
    path('whoami/', MELIWhoamIApi.as_view()),
    path('notify/', MELINotify.as_view()),
    path('viewsets/', include(router.urls))
]
