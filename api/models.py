from django.db import models


# Create your models here.
class MELIParams(models.Model):
    redirect_uri = models.URLField()
    code = models.CharField(max_length=50, null=True)
    token = models.CharField(max_length=100, null=True)
    refresh_token = models.CharField(max_length=100, null=True)
    seller = models.BigIntegerField(null=True)
    client = models.BigIntegerField(null=True)
    client_secret = models.CharField(max_length=100, null=True)

# REDIRECT_URI = 'https://0555bad6149f.ngrok.io/notifications/connect-meli-api/'
# CODE_CONSTANT = 'TG-5fbec972c3a71d00061712ea-252784217'
# TOKEN = 'APP_USR-422816225915832-112521-4a08023eaf016ca561bd4732885255eb-252784217'
# SELLER_ID = '252784217'
# CLIENT_ID = '422816225915832'
